set nocompatible
call plug#begin()
" On-demand loading
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }
Plug 'fatih/molokai'

Plug 'airblade/vim-gitgutter'
Plug 'ervandew/supertab'
Plug 'vim-airline/vim-airline'

Plug 'ctrlpvim/ctrlp.vim'
Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'tpope/vim-fugitive'
Plug 'majutsushi/tagbar'
Plug 'deoplete-plugins/deoplete-go', { 'do': 'make'}
" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()

" vim-go settings
set autowrite
let g:go_auto_sameids = 1
let g:go_def_mode='gopls'
let g:go_doc_popup_window = 1
let g:go_fmt_command = "goimports"
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_operators = 1
let g:go_highlight_types = 1
let g:go_info_mode='gopls'
let g:go_list_type = "quickfix"
let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
let g:molokai_original = 1
let g:rehash256 = 1
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>
let g:go_debug_windows = {
      \ 'vars':       'rightbelow 60vnew',
      \ 'stack':      'rightbelow 10new',
\ }
let g:go_debug_mappings = {
      \ '(go-debug-continue)': {'key': 'c', 'arguments': '<nowait>'},
      \ '(go-debug-next)': {'key': 'n', 'arguments': '<nowait>'},
      \ '(go-debug-print)': {'key': 'p'},
      \ '(go-debug-step)': {'key': 's'},
      \ '(go-debug-stepout)': {'key': 'S'},
  \}

" Path to python interpreter for neovim
let g:python3_host_prog  = '/usr/bin/python3'
" Skip the check of neovim module
let g:python3_host_skip_check = 1

" deoplete.nvim recommend
set completeopt+=noinsert
" set completeopt+=menuone,noselect,noinsert
" Run deoplete.nvim automatically
let g:deoplete#enable_at_startup = 1
"call deoplete#custom#option('omni_patterns', { 'go': '[^. *\t]\.\w*' })
" deoplete-go settings
let g:deoplete#sources#go#gocode_binary = '/usr/bin/gocode'
let g:deoplete#sources#go#sort_class = ['package', 'func', 'type', 'var', 'const']


" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
let NERDTreeQuitOnOpen = 1
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Build & test
map <F5> :GoDebugStart<CR>
map <F6> :GoDebugStop<CR>
map <F7> :GoDebugTest<CR>
"map <F8> :GoTest<CR>
nmap <F8> :TagbarToggle<CR>
map <F9> :GoDebugBreakpoint<CR>
map <C-z> :NERDTreeToggle<CR> “ Toggle side window with `CTRL+z`.
"let g:airline_powerline_fonts = 1

" my settings
colorscheme slate
set autoindent
set encoding=utf-8
set expandtab
set fileencoding=utf-8
set hlsearch
set ttymouse=sgr
set mouse=a
set number
"set paste
set shiftwidth=4
set showmatch
set smarttab
set softtabstop=4
set undodir=~/.vim/undodir
set undofile
syntax on

autocmd BufNewFile,BufRead /dev/shm/gopass* setlocal noswapfile nobackup noundofile viminfo=""
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4 
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

