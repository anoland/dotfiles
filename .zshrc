# Lines configured by zsh-newuser-install
setopt appendhistory beep extendedglob nomatch notify prompt_subst

bindkey -e
# End of lines configured by zsh-newuser-install
setopt histignorealldups
# The following lines were added by compinstall
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle :compinstall filename '/home/anoland/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
typeset -U path
path+=($HOME/projects/bin /usr/local/go/bin $HOME/.local/bin ${KREW_ROOT:-$HOME/.krew}/bin) 
export PATH
export GOPATH=$(go env GOPATH)

PS1="%B%F{blue}%3~%f%b 
%#"

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
PROMPT="%B%F{white}\$(kubectl config current-context ): %f\$vcs_info_msg_0_$PROMPT"
zstyle ':vcs_info:git:*' formats '%B%F{202}(%r@%b)%f '
zstyle ':vcs_info:*' enable git 

source $HOME/.aliases

source <(buf completion zsh)
source <(flux completion zsh)
source <(k3d completion zsh)
source <(kubectl completion zsh)
source <(tilt completion zsh)
source <(vcluster completion zsh)

eval `keychain --inherit any-once --quiet --eval --agents ssh,gpg ~/.ssh/id_ed25519 \
	B58CF5557C0F5543758DA25239F3013709D86729 96E679E49DD26A228EF0C8F4A3EB00F18C8611B6 `

eval "$(direnv hook zsh)"
