
export TERM=xterm-256color
export PROMPT='%m:%B%F{blue}%4d%f%b %# '
export HISTFILE=~/.histfile
export HISTSIZE=5000
export SAVEHIST=5000
export EDITOR=vim
export GIT_EDITOR=$EDITOR
export K9S_EDITOR=$EDITOR
export GPG_TTY=$(tty)
export GIT_TERMINAL_PROMPT=1

export GITHUB_TOKEN=$(gopass show -o git/github.com/anoland)
#export STEAM_RUNTIME=0
export LD_LIBRARY_PATH="/usr/lib:$LD_LIBRARY_PATH"

#export DOCKER_HOST=tcp://localhost:2376
#export DOCKER_TLS_VERIFY=1
#export DOCKER_CERT_PATH=~/.docker

export SUMMON_PROVIDER=gopass
